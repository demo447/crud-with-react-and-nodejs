import React, { Component } from 'react'
import Navbar from './navbar';
import { Alert } from 'reactstrap';
export default class home extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <br />
                <Alert color="success">
                    <h1>THIS IS A DEMO WEBSITE CREATED FOR THE BACKEND OF LOGIN SYSTEM CREATED IN NODE JS....</h1>
                </Alert>
                <br />
                <Alert color="success">
                    <h1><a href="/login">LOGIN PAGE</a></h1>
                </Alert>
                <br />
                <Alert color="success">
                    <h1><a href="/register">REGISTER</a></h1>
                </Alert>
            </div>
        )
    }
}
