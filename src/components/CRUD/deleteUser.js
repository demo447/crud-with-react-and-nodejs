import React, { Component } from 'react';
import { Alert } from 'reactstrap';
import Navcrud from './navcrud';

export default class deleteUser extends Component {
    render() {
        return (
            <>
            <Navcrud />
                <Alert color="success">
                   <h1>THE DATA IS BEEN DELETED SUCCESSFULLY......</h1>
                </Alert>

                <Alert color="success">
                   <h1><a href="/allUsers">TOTAL USERS</a></h1>
                </Alert>

                <br />

                <Alert color="success">
                   <h1><a href="/createUser">ADD A NEW USER INTO DB</a></h1>
                </Alert>

                <br />

                <br />
            </>
        )
    }
}
