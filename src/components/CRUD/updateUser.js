import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';
import axios from 'axios';
import baseUrl from './baseUrl';
import Navcrud from './navcrud';


export default class updateUser extends Component {
    constructor(props){
        super(props);
        let id= this.props.match.params.id;
        let u = baseUrl+"/api/new/"+id;
        axios.get(u).then(data => {
            console.log(data.data);
            this.setState({
                name: data.data[0].name,
                email: data.data[0].email,
                password: data.data[0].password
            });
        }).catch(error => {
            console.log(error);
        });



        this.state = {
            heading: "PLEASE ENTER THE NEW DATA....",
        }
    }
    componentDidMount(){

    }


    handleChangeName = (e) => {
        this.setState({
            name: e.target.value
        });
    }
    handleChangeUsername = (e) => {
        this.setState({
            email: e.target.value
        });
    }
    handleChangePassword = (e) => {
        this.setState({
            password: e.target.value
        });
    }
    handleButtonClick = () => {
        alert("data recorder");
    }
    handleForm = (e) => {
        e.preventDefault();
        const data = {
            userid: this.props.match.params.id,
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        }
        axios.put(`${baseUrl}/api/new/update`, data).then(data => {
            console.log(data);
            this.setState({
                heading: "YOUR DATA IS UPDATED SUCCESSFULLY...",
                name: "",
                email: "",
                password: ""
            });
        }).catch(error => {
            console.log(error);
        });
    }


    render() {
        return (
            <div>
                <Navcrud />
                <Alert color="success">
                   <h1>
                       {this.state.heading}
                   </h1>
                 </Alert>
                 <Form onSubmit = {this.handleForm}>
                     <FormGroup row>
                        <Label for="exampleEmail" sm={2}>NAME : </Label>
                        <Col sm={10}>
                        <Input type="text" name="name" value={this.state.name} id="exampleEmail" placeholder="enter your password..." onChange = {this.handleChangeName}/>
                        </Col>
                    </FormGroup>
                    <br/>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>EMAIL : </Label>
                        <Col sm={10}>
                        <Input type="text" name="email" id="exampleEmail" value={this.state.email} placeholder="enter your username" onChange = {this.handleChangeUsername}/>
                        </Col>
                    </FormGroup>
                    <br/>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>PASSWORD : </Label>
                        <Col sm={10}>
                        <Input type="password" name="password" id="exampleEmail" value={this.state.password} placeholder="enter your password..." onChange = {this.handleChangePassword}/>
                        </Col>
                    </FormGroup>
                    <br/>
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 2 }}>
                        <Button type="submit" onClick={this.handleButtonClick}>Submit</Button>
                        </Col>
                    </FormGroup>
                    </Form>
            </div>
        )
    }
}
