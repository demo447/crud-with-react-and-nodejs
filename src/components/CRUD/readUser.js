import axios from 'axios';
import React, { Component } from 'react';
import baseUrl from './baseUrl';
import { Button } from 'reactstrap';
import Navcrud from './navcrud';


export default class readUser extends Component {
    componentDidMount(){
        axios.get('/api/new/allData').then(
            (response) => {
                console.log(response.data);
                this.setState({allUser: response.data});
            },
            (error) => {
                console.log(error);
            }
        );
    }
    constructor(props){
        super(props);
        this.state = {
            allUser : []
        }
    }
    handleButtonDelete = (id) => {
        const deleteUrl = baseUrl+"/api/new/delete/"+id;
        axios.delete(`${deleteUrl}`);
    }
    render() 
    {
        return (
            <div>
                <Navcrud />
                {
                    this.state.allUser.map((data, key) => {
                        const url = "/delete/"+data.userid;
                        const updateUrl = "/update/"+data.userid;
                        const viewUrl = "/view/"+data.userid;
                        return <div key={key}>
                                    <h1>USER ID : {data.userid}</h1>
                                    <h1>USER NAME : {data.name}</h1>
                                    <h1>EMAIL : {data.email}</h1>
                                    <h1>PASSWORD : {data.password}</h1>
                                    <br />
                                    <Button color="secondary"><a href={viewUrl}>VIEW</a></Button>{'  '}
                                    <Button color="success"><a href={updateUrl}>UPDATE</a></Button>{'  '}
                                    <Button color="danger" onClick={() => {this.handleButtonDelete(data.userid)}}><a href={url}>DELETE</a></Button>
                                    <hr />
                                </div>
                    })
                }
            </div>
        )
    }
}
