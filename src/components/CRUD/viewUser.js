import axios from 'axios';
import React, { Component } from 'react';
import baseUrl from './baseUrl';
import Navcrud from './navcrud';
import { Alert, Button } from 'reactstrap';

export default class viewUser extends Component {
    componentDidMount(){
        let id= this.props.match.params.id;
        let u = baseUrl+"/api/new/"+id;
        axios.get(u).then(data => {
            this.setState({
                userid: data.data[0].userid,
                name: data.data[0].name,
                email: data.data[0].email,
                password: data.data[0].password
            });
        }).catch(error => {
            console.log(error);
        });
    }
    constructor(){
        super();
        this.state={
            userid: 0,
            name: "",
            email: "",
            password: ""
        };
    }
    handleButtonDelete = (id) => {
        const deleteUrl = baseUrl+"/api/new/delete/"+id;
        axios.delete(`${deleteUrl}`);
    }
    render() {
        const url = "/delete/"+this.state.userid;
        const updateUrl = "/update/"+this.state.userid;
        return (
            <div>
                <Navcrud />

                <Alert color="success">
                   <h1>WELCOME TO DASHBOARD</h1>
                </Alert>

                <br/>

                <Alert color="success">
                   <h2>HELLO {this.state.name}</h2>
                   <h2>YOUR EMAIL IS : {this.state.email}</h2>
                   <h2>IF YOU WANT TO UPDATE OR DELETE YOUR DATA....</h2>

                    <Button color="success"><a href={updateUrl}>UPDATE</a></Button>{'  '}
                    <Button color="danger" onClick={() => {this.handleButtonDelete(this.state.userid)}}><a href={url}>DELETE</a></Button>
                </Alert>

            </div>
        )
    }
}
