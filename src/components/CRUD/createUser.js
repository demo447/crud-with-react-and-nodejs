import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';
import axios from 'axios';
import baseUrl from './baseUrl';
import Navcrud from './navcrud';

export default class createUser extends Component {
    constructor(props){
        super(props);
        this.state = {
            email : "",
            password : "",
            name: ""
        };
    }
    handleChangeName = (e) => {
        this.setState({
            name: e.target.value
        });
    }
    handleChangeUsername = (e) => {
        this.setState({
            email: e.target.value
        });
    }
    handleChangePassword = (e) => {
        this.setState({
            password: e.target.value
        });
    }
    handleButtonClick = () => {
        alert("data recorder");
    }
    handleForm = (e) => {
        e.preventDefault();
        const data = {
           name: this.state.name,
           email: this.state.email,
           password: this.state.password
        }
        console.log(data);
        axios.post(`/api/new/addData`, data).then((data) => {
            console.log(data);
            this.setState({
                name: "",
                email: "",
                password: ""
            });
        });
    }
    render() {
        return (
            <div>
                <Navcrud />
                  <Alert color="success">
                   <h1>PLEASE ENTER THE FOLLOWING DETAILS BELOW....</h1>
                 </Alert>
                 <Form onSubmit = {this.handleForm}>
                     <FormGroup row>
                        <Label for="exampleEmail" sm={2}>NAME : </Label>
                        <Col sm={10}>
                        <Input type="text" name="name" value={this.state.name} id="exampleEmail" placeholder="enter your password..." onChange = {this.handleChangeName}/>
                        </Col>
                    </FormGroup>
                    <br/>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>EMAIL : </Label>
                        <Col sm={10}>
                        <Input type="text" name="email" id="exampleEmail" value={this.state.email} placeholder="enter your username" onChange = {this.handleChangeUsername}/>
                        </Col>
                    </FormGroup>
                    <br/>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>PASSWORD : </Label>
                        <Col sm={10}>
                        <Input type="password" name="password" id="exampleEmail" value={this.state.password} placeholder="enter your password..." onChange = {this.handleChangePassword}/>
                        </Col>
                    </FormGroup>
                    <br/>
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 2 }}>
                        <Button type="submit" onClick={this.handleButtonClick}>Submit</Button>
                        </Col>
                    </FormGroup>
                    </Form>
            </div>
        )
    }
}
