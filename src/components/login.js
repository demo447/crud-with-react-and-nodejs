import React, { Component } from 'react';
import Navbar from "./navbar";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';

export default class login extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <Alert color="success">
                   <h1>PLEASE ENTER YOUR LOGIN CREDINTIALS....</h1>
                </Alert>
                 <Form>
                    <br/>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>EMAIL : </Label>
                        <Col sm={10}>
                        <Input type="text" name="email" id="exampleEmail" placeholder="enter your username" />
                        </Col>
                    </FormGroup>
                    <br/>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>PASSWORD : </Label>
                        <Col sm={10}>
                        <Input type="password" name="password" id="exampleEmail" placeholder="enter your password..." />
                        </Col>
                    </FormGroup>
                    <br/>
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 2 }}>
                        <Button>Submit</Button>
                        </Col>
                    </FormGroup>
                    </Form>
            </div>
        )
    }
}
