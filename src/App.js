import './App.css';
import Register from './components/register';
import {Route, Switch } from 'react-router-dom';
import login from './components/login';
import home from './components/home';
import crud from './components/crud';
import readUser from './components/CRUD/readUser';
import createUser from './components/CRUD/createUser';
import deleteUser from './components/CRUD/deleteUser';
import updateUser from './components/CRUD/updateUser';
import viewUser from './components/CRUD/viewUser';


function App() {
  return (
    <div className="App">
      <main>
            <Switch>
                <Route path="/" component={home} exact />
                <Route path="/login" component={login} />
                <Route path="/register" component={Register} />
                <Route path="/crud" component={crud} />
                <Route path="/allUsers" component={readUser} />
                <Route path="/createUser" component={createUser} />
                <Route path="/delete/:id" component={deleteUser} />
                <Route path="/update/:id" component={updateUser} />
                <Route path="/view/:id" component={viewUser} />
            </Switch>
        </main>
    </div>
  );
}

export default App;
